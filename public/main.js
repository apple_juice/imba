"use strict";

window.addEventListener('DOMContentLoaded', function () {
  // START OF: is mobile =====
  function isMobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }
  // ===== END OF: is mobile

  const BODY = $('body');

  // START OF: mark is mobile =====
  (function() {
    BODY.addClass('loaded');

    if(isMobile()){
      BODY.addClass('body--mobile');
      $('video').remove()
    }else{
      BODY.addClass('body--desktop');
    }
  })();

  $('.animation').one('inview', function (event, isInView) {
    if (isInView == true) {
      $(this).addClass('inView');
    }
  });

  let menu = (function(){
    let sidebar = $('.sidebar');
    let sidebarLinks = $('.nav-item a', sidebar);
    let btn = $('.btn_menu');

    function init(){
      btn.on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        BODY.toggleClass('menu-opened');
        $(this).toggleClass('active');
        sidebar.toggleClass('sidebar--show');
      });

      sidebarLinks.on('click', function (e) {
        BODY.removeClass('menu-opened');
        btn.removeClass('active');
        sidebar.removeClass('sidebar--show');
      });

      sidebar.on('click', function (e) {
        e.stopPropagation();
      })

    }
    return {
      init: init
    }
  }());

  menu.init();

  $(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on('click', function (e) {
      $('[data-toggle="popover"],[data-original-title]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
        }

      });
    });

    var is_touch_device = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch;
    $('[data-toggle="popover"]').popover({
      trigger: is_touch_device ? "click" : "click hover"
    });

    $('._slider_reviews_bg').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '._slider_reviews_items'
    });

    $('._slider_reviews_items').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: '._slider_reviews_bg',
      dots: false,
      centerMode: true,
      focusOnSelect: true,
      fade: true,
    });

    $('._slider_info_bg').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '._slider_mobile',
      mobileFirst: true,
      responsive: [
        {
          breakpoint: 991,
          settings: "unslick"
        },
      ]
    });

    $('._slider_mobile').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: '._slider_info_bg',
      mobileFirst: true,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 991,
          settings: "unslick"
        },
      ]
    });

    $('._slider_items').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      dots: false,
      responsive: [
        {
          breakpoint: 767,
          settings: "unslick"
        },
      ]
    });

    $('._slider_inside').slick({
      mobileFirst: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: "unslick"
        },
      ]
    });

    $('._slider_product_info').slick({
      mobileFirst: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 1080,
          settings: "unslick"
        },
      ]
    });

    $('._slider_items_row').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      dots: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  });


  $(document).ready(function(){
    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrolltop_btn').fadeIn();
      } else {
        $('.scrolltop_btn').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrolltop_btn').click(function(){
      $('html, body').animate({scrollTop : 0},800);
      return false;
    });

  });
});